package com.example.contacts;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private ContactViewModel viewModel;
    private PagedContactsAdapter adapter2;
    private ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        instantiateData();

        recycler.setAdapter(adapter2);
        recycler.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        viewModel.liveData.observe(this, new Observer<PagedList<ContactSimpleModel>>() {
            @Override
            public void onChanged(PagedList<ContactSimpleModel> contactSimpleModels) {
                if (contactSimpleModels != null && contactSimpleModels.size() > 0) // hide loading when data is ready
                    if (pb.getVisibility() == View.VISIBLE)
                        pb.setVisibility(View.GONE);
                adapter2.submitList(contactSimpleModels);

            }
        });

        checkAndRequestPermission();
    }

    private void instantiateData() {
        viewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        adapter2 = new PagedContactsAdapter();
    }

    private void startWatcherService() {
        ContactWatcherService service = new ContactWatcherService();
        service.onStartCommand(null, 0, 0);
    }

    private void checkAndRequestPermission() {
        if (!hasPermission()) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    255);
        } else {
            startWatcherService();
        }
    }

    private boolean hasPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
            return false;
        else
            return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            checkAndRequestPermission();
        } catch (Exception e){
            Toast.makeText(this, "Permission Denied Continuously", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void findViews() {
        recycler = findViewById(R.id.recycler);
        pb = findViewById(R.id.pb);
    }
}
