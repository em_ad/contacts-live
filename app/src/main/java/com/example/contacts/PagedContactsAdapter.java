package com.example.contacts;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

public class PagedContactsAdapter extends PagedListAdapter<ContactSimpleModel, PagedContactsAdapter.ViewHolder> {


    protected PagedContactsAdapter() {
        super(new DiffUtil.ItemCallback<ContactSimpleModel>() {
            @Override
            public boolean areItemsTheSame(@NonNull ContactSimpleModel oldItem, @NonNull ContactSimpleModel newItem) {
                if (newItem.getId().equals(oldItem.getId()))
                    return true;
                return false;
            }

            @Override
            public boolean areContentsTheSame(@NonNull ContactSimpleModel oldItem, @NonNull ContactSimpleModel newItem) {
                if (newItem.primaryName.equals(oldItem.primaryName) && newItem.number.equals(oldItem.number))
                    return true;
                return false;
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ContactSimpleModel model = getItem(position);
        if (model != null) {
            holder.tvName.setText(model.primaryName);
            holder.tvNumber.setText(model.number);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvNumber;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvNumber = itemView.findViewById(R.id.tvNumber);
        }
    }
}
