package com.example.contacts;

import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface ContactsDao {
    @Query("SELECT * FROM contactsdb")
    abstract DataSource.Factory<Integer, ContactSimpleModel> getAll();

    @Query("SELECT * FROM contactsdb")
    List<ContactSimpleModel> getAllSimple();

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ContactSimpleModel> contacts);

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insert(ContactSimpleModel contacts);

    @Update
    void updateAll(List<ContactSimpleModel> contacts);

    @Delete
    void delete(ContactSimpleModel user);

    @Query("DELETE FROM contactsdb")
    public void deleteAll();

//
//    @Query("SELECT * FROM contactsdb")
//    LiveData<List<ContactSimpleModel>> getAll();
//
//    @Insert (onConflict = OnConflictStrategy.REPLACE)
//    void insertAll(List<ContactSimpleModel> contacts);
//
//    @Delete
//    void delete(ContactSimpleModel user);
//
//    @Query("DELETE FROM contactsdb")
//    public void deleteAll();
}

