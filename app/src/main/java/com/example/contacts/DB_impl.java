package com.example.contacts;

import androidx.room.Room;

public class DB_impl
{
    private static DB_impl single_instance = null;

    DB db;

    private DB_impl()
    {
        db = Room.databaseBuilder(MyApplication.getContext(),
                DB.class, "myroom").build();
    }

    public static DB_impl getInstance()
    {
        if (single_instance == null)
            single_instance = new DB_impl();

        return single_instance;
    }
}
