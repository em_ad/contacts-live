package com.example.contacts;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import java.util.List;

public class ContactViewModel extends ViewModel {

    LiveData<PagedList<ContactSimpleModel>> liveData;


    public ContactViewModel() {
        liveData = new LivePagedListBuilder<>(DB_impl.getInstance().db.userDao().getAll(), 50).build();
    }

}
