package com.example.contacts;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import java.util.ArrayList;
import java.util.List;

public class ContactWatcherService extends Service {

    int crash = 0;
    static boolean busy = false;
    int lastSize = 0;
    java.util.concurrent.CopyOnWriteArrayList<ContactSimpleModel> contacts = new java.util.concurrent.CopyOnWriteArrayList<ContactSimpleModel>();
    ContentObserver contentObserver;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        ContentResolver cr = MyApplication.getContext().getContentResolver();
        contentObserver = new ContentObserver(null) {
            @Override
            public void onChange(boolean selfChange, Uri uri) {
                Log.e("tag", "Count: " + crash++);
                super.onChange(selfChange, uri);
                getContactsFromPhone();

            }
        };
        cr.registerContentObserver(
                ContactsContract.Contacts.CONTENT_URI,
                true,
                contentObserver);

        if (lastSize == 0)
            getContactsFromPhone();

        return Service.START_STICKY;
    }

    private void getContactsFromPhone() {

        contacts.clear();
        ContentResolver cr = MyApplication.getContext().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));
                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        ContactSimpleModel c = new ContactSimpleModel(id, name, phoneNo);
                        contacts.add(c);
//                        DB_impl.getInstance().db.userDao().insert(c);
                    }
                    pCur.close();
                }
            }
        }
        if (cur != null) {
            cur.close();
        }

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                while (busy) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                busy = true;
                List<ContactSimpleModel> mod = new ArrayList<>(); // NO LONGER EXIST
                mod = DB_impl.getInstance().db.userDao().getAllSimple();
                if (mod != null && !mod.isEmpty())
                    for (int i = 0; i < mod.size(); i++) {
                        boolean exists = false;
                        for (int j = 0; j < contacts.size(); j++) {
                            if (contacts.get(j).getId().equals(mod.get(i).getId())) {
                                exists = true;
                                break;
                            }
                        }
                        if (!exists)
                            DB_impl.getInstance().db.userDao().delete(mod.get(i));

                    }

                DB_impl.getInstance().db.userDao().insertAll(contacts);

                lastSize = contacts.size();
                busy = false;
                return null;
            }
        }.execute();

//        Thread t = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (busy) {
//                    try {
//                        wait();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//                busy = true;
//                List<ContactSimpleModel> mod = new ArrayList<>(); // NO LONGER EXIST
//                mod = DB_impl.getInstance().db.userDao().getAllSimple();
//                if (mod != null && !mod.isEmpty())
//                    for (int i = 0; i < mod.size(); i++) {
//                        boolean exists = false;
//                        for (int j = 0; j < contacts.size(); j++) {
//                            if (contacts.get(j).getId().equals(mod.get(i).getId())) {
//                                exists = true;
//                                break;
//                            }
//                        }
//                        if (!exists)
//                            DB_impl.getInstance().db.userDao().delete(mod.get(i));
//
//                    }
//                    DB_impl.getInstance().db.userDao().insertAll(contacts);
//
//                lastSize = contacts.size();
//                busy = false;
//            }
//        });
//        t.start();

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

