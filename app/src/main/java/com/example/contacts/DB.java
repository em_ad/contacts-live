package com.example.contacts;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {ContactSimpleModel.class}, version = 1)
public abstract class DB extends RoomDatabase {
    public abstract ContactsDao userDao();
}

