package com.example.contacts;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "contactsdb")
public class ContactSimpleModel implements Serializable {

    @PrimaryKey
    @NonNull
    String id;
    @ColumnInfo(name = "name")
    String primaryName;
    @ColumnInfo(name = "number")
    String number;

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getId() {
        return id;
    }

    public ContactSimpleModel(String id, String primaryName, String number) {
        this.id = id;
        this.primaryName = primaryName;
        this.number = number;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        ContactSimpleModel contact = (ContactSimpleModel) obj;
        return contact.getId().equals(this.id) && contact.getNumber().equals(this.number) && contact.getPrimaryName().equals(this.primaryName);
    }
}

